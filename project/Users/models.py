# -*- coding: utf-8 -*-
from mongoengine import *
import datetime

class User(Document):
    email = StringField(required=True)
    first_name = StringField(max_length=50)
    last_name = StringField(max_length=50)
    password = StringField(max_length=200)
    date_modified = DateTimeField(default=datetime.datetime.utcnow)
