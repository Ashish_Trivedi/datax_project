class Config(object):
    # ...
    DATABASE_URI = os.environ.get('DATABASE_URI') or \
        'mongodb://localhost:27017/DataX_test'