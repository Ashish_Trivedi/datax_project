FROM python:3.6
MAINTAINER Praveen Rewar "praveen.rewar@jaitra.com"
COPY . /app
WORKDIR /app
RUN pip install -r requirements.txt
ENTRYPOINT ["python"]
CMD ["runserver.py"]